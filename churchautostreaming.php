<?php
/**
* Plugin Name: Church Auto Streaming
* Plugin URI: https://churchautostreaming.com
* Description: Automaticly video and audio stream your church services to your website.
* Version: 0.1
* Author: Church Auto Streaming
* Author URI: https://churchautostreaming.com
* License: GPL2
*/


class churchautostreaming {
	public $baseDir = FALSE;
	public $streamDir = "/stream";
	public $startTime = 0;
	public $timer = array();
	
	public function __construct(){
		
		$this->startTime = time();
		$this->timer['construct'] = time() - $this->startTime;
		$this->baseDir = getcwd().'/wp-content/plugins/churchautostreaming';
		add_action('parse_request', array($this, 'sniff_requests'), 0);
		add_action('wp_enqueue_scripts', array($this,'my_custom_scripts'));
	}

	public function my_custom_scripts(){
		$baseURL = 'http://henryapostolictabernacle.org/wp-content/plugings/churchautostreaming/stream/';
		$get = array(
			'user_id'	=>	'5',
			'isLive'		=>	true,
			'mediaLib'	=>	array(
				'2016-02-14-am.mp4',
				'2016-02-14-pm.mp4',
				'2016-02-12-pm.mp4',
			)
		);
		$js = '//churchautostreaming.com/embed.js';
		wp_enqueue_script('cas_embed', $js);
		wp_localize_script( 'cas_embed', 'cas_embed', $get );
	}

	public function sniff_requests(){
		global $wp;
		if(isset($wp->query_vars['pagename'])){
			$page_parts = explode('/',$wp->query_vars['pagename']);
			//lets see if they are going to /churchautostreaming/
			if (isset($page_parts[0]) && $page_parts[0] == "churchautostreaming"){
				/*
				if(!is_admin()){
					die('you need to login!');	
				}
				*/
				$functionName = 'api_'.$page_parts[1];
				//lets see if the function they are calling is a real function (/churchautostreaming/upload)
				if (isset($page_parts[1]) && method_exists($this,$functionName)){
					// These files need to be included as dependencies when on the front end.
					$this->timer['bef-func'] = time() - $this->startTime;
					$this->$functionName();
				}
			}
		}
	}

	private function response($isError = TRUE, $return = array())
	{
		$this->timer['response'] = time() - $this->startTime;
		header('Content-Type: application/json');
		echo json_encode(array(
			'time'		=>	time(),
			'isError'	=>	$isError,
			'return'		=>	$return,
			'timer'		=>	$this->timer,
		));
		die();
	}

	/*
	*
	* API Functions
	*
	*/

	//https://github.com/WP-API/WP-API/blob/5801d44c54dbe33197915df1e9cdc3dc156f26b5/lib/endpoints/class-wp-rest-attachments-controller.php#L56
	public function api_upload()
	{
		$this->timer['up-1'] = time() - $this->startTime;
		global $wp;
		$return = array();
		$isError = FALSE;
		//upload our files
		if (isset($_FILES['cas_file'])){
			if (!isset($_POST['cas_file_md5']) || !isset($_POST['cas_file_purpose'])){
				$this->response(TRUE,'Uploaded files need a MD5 and a purpose');
			}else{
				$isLive = FALSE;
				if ($_POST['cas_file_purpose'] == "live"){
					$isLive = TRUE;
				}
				$return = $this->handle_upload('cas_file',$_POST['cas_file_md5'],$isLive);
				$this->timer['up-2'] = time() - $this->startTime;
				if ($return['isError']){
					$isError = TRUE;
				}
				$this->response($isError,$return);
			}	
		}else{
			$this->response(TRUE,'No file to upload.');
		}
	}
	
	public function api_synclist()
	{
		global $wp;
		$return = array(1,2,3);
		$isError = FALSE;
		$this->verifyFolers();
		$fileList = scandir($this->baseDir.$this->streamDir);
		$files = array();
		foreach ($fileList as $id => $name){
			if ($name != "." && $name != ".."){
				$files[$name] = md5_file($this->baseDir.$this->streamDir.'/'.$name);
			}
		}
		$this->response($isError,$files);
	}
	
	public function api_cleanup()
	{
		global $wp;
		$fileList = scandir($this->baseDir.$this->streamDir);
		$files = array();
		foreach ($fileList as $id => $name){
			if ($name != "." && $name != ".."){
				unlink($this->baseDir.$this->streamDir.'/'.$name);
			}
		}
		$this->response(FALSE);
	}
	
	private function verifyFolers()
	{
		$this->timer['vf-1'] = time() - $this->startTime;
		$isError = FALSE;
		if (!is_dir($this->baseDir) || !is_writeable($this->baseDir)){
			$isError = TRUE;
		}else{
			if (!is_dir($this->baseDir.$this->streamDir)){
				mkdir($this->baseDir.$this->streamDir);
			}else{
				$isError - TRUE;
			}	
		}
		if ($isError){
			$this->response($isError,'folders need help');
		}
		$this->timer['vf-2'] = time() - $this->startTime;
	}

	private function handle_upload($filename,$fileMD5,$isLive = FALSE)
	{
		$this->timer['hu-1'] = time() - $this->startTime;
		$this->verifyFolers();
		$return				=	array();
		$return['isError']	=	FALSE;
		if (!$isLive){
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			$return['attachment_id'] = media_handle_upload($filename, 0);
			$return['url'] = wp_get_attachment_url($return['attachment_id']);
			$return['isError'] = is_wp_error($attachment_id);
		}else{
			move_uploaded_file($_FILES[$filename]['tmp_name'],$this->baseDir.$this->streamDir.'/'.$_FILES[$filename]['name']);	
			$this->timer['hu-2'] = time() - $this->startTime;
		}
		//TODO: verify md5 of file
		return $return;
	}
}

new churchautostreaming();
